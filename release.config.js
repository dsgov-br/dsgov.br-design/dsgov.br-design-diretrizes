const { branches, changelog, commitAnalyzer, git, gitlab, releaseNotesGenerator } = require('@govbr-ds/release-config')

module.exports = {
  branches: branches,
  plugins: [
    commitAnalyzer,
    releaseNotesGenerator,
    changelog,
    [
      '@semantic-release/exec',
      {
        prepareCmd: 'pnpm exec nx release version ${nextRelease.version}',
        publishCmd: 'pnpm exec nx release publish --tag ${nextRelease.channel}',
      },
    ],
    git,
    gitlab,
  ],
}
